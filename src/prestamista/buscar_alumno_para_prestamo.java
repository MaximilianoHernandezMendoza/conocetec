package prestamista;

import conocetec.Administrador.*;
import conocetec.metodossql.metodos;
import javax.swing.JOptionPane;

public class buscar_alumno_para_prestamo extends javax.swing.JFrame {
    String matri = "";
    boolean ir;
    public buscar_alumno_para_prestamo() {
        initComponents();
        setLocationRelativeTo(null); // localiza la panalla en el centro :V
    }

    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        txtmatricula = new javax.swing.JTextField();
        bottguardar = new javax.swing.JToggleButton();
        bottcancelar = new javax.swing.JToggleButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jLabel1.setText("Matricula:");

        txtmatricula.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtmatriculaKeyTyped(evt);
            }
        });

        bottguardar.setText("Buscar");
        bottguardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bottguardarActionPerformed(evt);
            }
        });

        bottcancelar.setText("cancelar");
        bottcancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bottcancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(jLabel1)
                .addGap(33, 33, 33)
                .addComponent(txtmatricula, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(52, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(bottguardar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(bottcancelar)
                .addGap(29, 29, 29))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtmatricula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 60, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bottguardar)
                    .addComponent(bottcancelar))
                .addGap(40, 40, 40))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtmatriculaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmatriculaKeyTyped
        // TODO add your handling code here:
        char car = evt.getKeyChar(); // usa el codigo ascci
        if (txtmatricula.getText().length() >= 8) {
            evt.consume(); // establece a solo 8 numero
        }
        if ((car < '0' || car > '9')) { // solo acepta numeros
            evt.consume(); // consume si no son numeros
        }

    }//GEN-LAST:event_txtmatriculaKeyTyped

    private void bottcancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bottcancelarActionPerformed
       if(ir == true){//si el booleano es verdadero se va a la pagina principal del administrador
      // pantalla_admi pan = new pantalla_admi();
       prestarlibro pre = new prestarlibro();
       pre.setVisible(false);
       //pan.setVisible(true);
       this.dispose();       
       }
       if(ir == false){ // si el booleano es falso se va la pagina de prestar
       prestarlibro pan = new prestarlibro();
       prestarlibro pre = new prestarlibro();
       pre.setVisible(false);
       pan.setVisible(true);
       this.dispose();           
       }
    }//GEN-LAST:event_bottcancelarActionPerformed

    private void bottguardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bottguardarActionPerformed
        // TODO add your handling code here:
        metodos met = new metodos();
        String completo = met.busquedanombrealumno(txtmatricula.getText());
       if(completo.length() > 0){
        prestarlibro pre = new prestarlibro();
        pre.buscaralumno(txtmatricula.getText());
        pre.setVisible(true);
        this.dispose();
       }else {
           JOptionPane.showMessageDialog(null, "Alumno no encontrado");
       }
    }//GEN-LAST:event_bottguardarActionPerformed

    public void boll(boolean vol){
        ir = vol;
    }
    
    
    
    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(buscar_alumno_para_prestamo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(buscar_alumno_para_prestamo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(buscar_alumno_para_prestamo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(buscar_alumno_para_prestamo.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new buscar_alumno_para_prestamo().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton bottcancelar;
    private javax.swing.JToggleButton bottguardar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JTextField txtmatricula;
    // End of variables declaration//GEN-END:variables
}

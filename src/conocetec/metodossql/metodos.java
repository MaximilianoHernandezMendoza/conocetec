package conocetec.metodossql;

import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
//import conocetec.metodossql.conexionBI;

public class metodos extends conexionBI {

    public static conexionBI conexion = new conexionBI();
    public static conexionBI conexion1 = new conexionBI();
    public static PreparedStatement sentencia_preparada;
    public static ResultSet resultado;
    public static String sql;
    public static int resultado_numero = 0;

    public int guardarPreOAdmi(String idusuarios, String nombre, String apellidos, String contrasena, String telefono, String tipoUs) {
        int resultado = 0;
        Connection conexion = null;

        String sentencia_guardada = "insert into usuarios(idUsuarios,nombre,apellidos,contrasena,telefono,tipo_de_usuario) values (?,?,?,?,?,?);";
        try {
//            conexion = conexionBD.getConexion();
            conexion = conexionBI.conector();
            sentencia_preparada = conexion.prepareStatement(sentencia_guardada);
            sentencia_preparada.setString(1, idusuarios);
            sentencia_preparada.setString(2, nombre);
            sentencia_preparada.setString(3, apellidos);
            sentencia_preparada.setString(4, contrasena);
            sentencia_preparada.setString(5, telefono);
            sentencia_preparada.setString(6, tipoUs);
            resultado = sentencia_preparada.executeUpdate();
            sentencia_preparada.close();//cierra conexion
            conexion.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        return resultado;
    }
    public int sumarlibro(String idusuarios, String nombre, String apellidos, String contrasena, String telefono, String tipoUs) {
        int resultado = 0;
        Connection conexion = null;

        String sentencia_guardada = "insert into usuarios(idUsuarios,nombre,apellidos,contrasena,telefono,tipo_de_usuario) values (?,?,?,?,?,?);";
        try {
//            conexion = conexionBD.getConexion();
            conexion = conexionBI.conector();
            sentencia_preparada = conexion.prepareStatement(sentencia_guardada);
            sentencia_preparada.setString(1, idusuarios);
            sentencia_preparada.setString(2, nombre);
            sentencia_preparada.setString(3, apellidos);
            sentencia_preparada.setString(4, contrasena);
            sentencia_preparada.setString(5, telefono);
            sentencia_preparada.setString(6, tipoUs);
            resultado = sentencia_preparada.executeUpdate();
            sentencia_preparada.close();//cierra conexion
            conexion.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        return resultado;
    }

    public int prestar(String folio, String fechasa, String fechaen, String matricula, String libro, String usuario) {
        int resultado = 0;
        Connection conexion = null;

        String sentencia_guardada = "insert into prestamo(FolioPre,FechaSa,FechaEn,Alumnos_Matricula,Libro_idlibro,Usuarios_idUsuarios) values (?,?,?,?,?,?);";
        try {
//            conexion = conexionBD.getConexion();
            conexion = conexionBI.conector();
            sentencia_preparada = conexion.prepareStatement(sentencia_guardada);
            sentencia_preparada.setString(1, folio);
            sentencia_preparada.setString(2, fechasa);
            sentencia_preparada.setString(3, fechaen);
            sentencia_preparada.setString(4, matricula);
            sentencia_preparada.setString(5, libro);
            sentencia_preparada.setString(6, usuario);
            resultado = sentencia_preparada.executeUpdate();
            sentencia_preparada.close();//cierra conexion
            conexion.close();
        } catch (Exception e) {
            System.out.println("prestar "+e);
        }
        return resultado;
    }
    
    public int guardar_editorial(String editorial) {// funciona
        Connection conexion = null;
        int resultado = 0;
        String sentencia_guardada = "insert into editorial values (?);";
        try {
            // se registra la editorial
            conexion = conexionBI.conector();
            sentencia_preparada = conexion.prepareStatement(sentencia_guardada);
            sentencia_preparada.setString(1, editorial);
            resultado = sentencia_preparada.executeUpdate();
            sentencia_preparada.close();//cierra conexion
            conexion.close();
        } catch (Exception e) {
            System.out.println("el error de editorial es: " + e);
        }
        return resultado;
    }
    
    public int eliminarlibro(String idlibro) {// funciona
        Connection conexion = null;
        int resultado = 0;
        String sentencia_guardada = "DELETE FROM libro WHERE idlibro = ?;";
        try {
            // se registra la editorial
            conexion = conexionBI.conector();
            sentencia_preparada = conexion.prepareStatement(sentencia_guardada);
            sentencia_preparada.setInt(1, Integer.parseInt(idlibro));
            resultado = sentencia_preparada.executeUpdate();
            System.out.println("el resultado es: "+ resultado);
            sentencia_preparada.close();//cierra conexion
            conexion.close();
        } catch (Exception e) {
            System.out.println("el error al eliminar libro es: " + e);
        }
        return resultado;
    }
    public int eliminaralumno(String matricula) {// funciona
        Connection conexion = null;
        int resultado = 0;
        String sentencia_guardada = "DELETE FROM alumnos WHERE Matricula = ?;";
        try {
            // se registra la editorial
            conexion = conexionBI.conector();
            sentencia_preparada = conexion.prepareStatement(sentencia_guardada);
            sentencia_preparada.setInt(1, Integer.parseInt(matricula));
            resultado = sentencia_preparada.executeUpdate();
            System.out.println("el resultado es: "+ resultado);
            sentencia_preparada.close();//cierra conexion
            conexion.close();
            System.out.println("eliminarususario " + "resultado");
        } catch (Exception e) {
            System.out.println("eliminaralumno : " + e);
        }
        return resultado;
    }
        public int elimiarmulta(String matricula) {// funciona
        Connection conexion = null;
        int resultado = 0;
        String sentencia_guardada = "DELETE FROM adeudos WHERE Alumnos_Matricula = ?;";
        try {
            // se registra la editorial
            conexion = conexionBI.conector();
            sentencia_preparada = conexion.prepareStatement(sentencia_guardada);
            sentencia_preparada.setInt(1, Integer.parseInt(matricula));
            resultado = sentencia_preparada.executeUpdate();
            System.out.println("el resultado es: "+ resultado);
            sentencia_preparada.close();//cierra conexion
            conexion.close();
            System.out.println("eliminarususario " + "resultado");
        } catch (Exception e) {
            System.out.println("eliminarmulta : " + e);
        }
        return resultado;
    }
    public int eliminarusuariosalumno(String matricula) {// funciona
        Connection conexion = null;
        int resultado = 0;
        String sentencia_guardada = "DELETE FROM usuarios WHERE idUsuarios = ?;";
        try {
            // se registra la editorial
            conexion = conexionBI.conector();
            sentencia_preparada = conexion.prepareStatement(sentencia_guardada);
            sentencia_preparada.setString(1, matricula);
            resultado = sentencia_preparada.executeUpdate();
            System.out.println("el resultado es: "+ resultado);
            sentencia_preparada.close();//cierra conexion
            conexion.close();
        } catch (Exception e) {
            System.out.println("eliminarusuarioalumno : " + e);
        }
        return resultado;
    }

    public int guardarLibro(String idlibro, String titulo, String ano, String autor, String noEjem,
            String estante, String editorial, String area) {// funciona
        int resultado = 0;
        Connection conexion = null;
        String sentencia_guardada2 = "insert into libro values (?,?,?,?,?,?,?,?);";
        try {
            // se registra el libro
            conexion = conexionBI.conector();
            sentencia_preparada = conexion.prepareStatement(sentencia_guardada2);
            sentencia_preparada.setString(1, idlibro);
            sentencia_preparada.setString(2, titulo);
            sentencia_preparada.setString(3, ano);
            sentencia_preparada.setString(4, estante);
            sentencia_preparada.setString(5, autor);
            sentencia_preparada.setString(6, noEjem);
            sentencia_preparada.setString(7, editorial);
            sentencia_preparada.setString(8, area);
            resultado = sentencia_preparada.executeUpdate();
            sentencia_preparada.close();//cierra conexion
            conexion.close();
            JOptionPane.showMessageDialog(null, "libro guardado");

        } catch (SQLException e) {
            System.out.println("el error libro es: " + e);
        }
        return resultado;
    }

    public int modificarLibro(String idlibro, String titulo, String ano, String autor, String noEjem, String estante, String editorial, String area) {// funciona
        int resultado = 0;
        Connection conexion = null;
        String sentencia_guardada2 = "UPDATE libro SET idlibro=?,Titulo=?,Ano=?,Estante=?,Autor=?,NoEjem=?,Editorial_idEditorial=?,carrera_idCarrera=? where idlibro = ?;";
        try {
            // se registra el libro
            conexion = conexionBI.conector();
            sentencia_preparada = conexion.prepareStatement(sentencia_guardada2);
            sentencia_preparada.setString(1, idlibro);
            sentencia_preparada.setString(2, titulo);
            sentencia_preparada.setString(3, ano);
            sentencia_preparada.setString(4, estante);
            sentencia_preparada.setString(5, autor);
            sentencia_preparada.setString(6, noEjem);
            sentencia_preparada.setString(7, editorial);
            sentencia_preparada.setString(8, area);
            sentencia_preparada.setString(9, idlibro);

            resultado = sentencia_preparada.executeUpdate();
            sentencia_preparada.close();//cierra conexion
            conexion.close();
            JOptionPane.showMessageDialog(null, "libro modificado");

        } catch (SQLException e) {
            System.out.println("el error libro es: " + e);
        }
        return resultado;
    }
    public void comboboxeditorial(JComboBox a) {// funciona
        Connection conexion = null;
        String sentencia_preparada = "select idEditorial from editorial;";
        try {
            conexion = conexionBI.conector();
            PreparedStatement pst = conexion.prepareStatement(sentencia_preparada);
            resultado = pst.executeQuery();
            while (resultado.next()) {
                a.addItem(resultado.getString("idEditorial"));
            }
            conexion.close();
        } catch (Exception e) {
            System.out.println("el error del comboBox del editorial es: ");
        }
    }

    public void comboboxareas(JComboBox a) {//funciona
        Connection conexion = null;
        String sentencia_preparada = "select idCarrera from Area;";
        try {
            conexion = conexionBI.conector();
            PreparedStatement pst = conexion.prepareStatement(sentencia_preparada);
            resultado = pst.executeQuery();
            while (resultado.next()) {
                a.addItem(resultado.getString("idCarrera"));
            }
            conexion.close();
        } catch (Exception e) {
            System.out.println("el error del comboBox de areas es: ");
        }
    }

    public int modificaralumno(String matricula, int semestre, String carrera) {// funciona
        int resultado = 0;
        Connection conexion = null;
        String sentencia_guardada2 = "UPDATE alumnos SET Matricula=?,Semestre=?,Carrera_idCarrera=?,Usuarios_idUsuarios=? where Matricula = ?;";
        try {
            // se registra el libro
            conexion = conexionBI.conector();
            sentencia_preparada = conexion.prepareStatement(sentencia_guardada2);
            sentencia_preparada.setInt(1, Integer.parseInt(matricula));
            sentencia_preparada.setInt(2, semestre);
            sentencia_preparada.setString(3, carrera);
            sentencia_preparada.setString(4, matricula);
            sentencia_preparada.setInt(5, Integer.parseInt(matricula));
            resultado = sentencia_preparada.executeUpdate();
            sentencia_preparada.close();//cierra conexion
            conexion.close();
//            JOptionPane.showMessageDialog(null, "Alumno Modificado");

        } catch (SQLException e) {
            System.out.println("modificaralumno: " + e);
        }
        return resultado;
    }
    public int modificarusuarioalumno(String idusuarios, String nombre, String apellidos, String telefono, String tipousuario) {// funciona
        int resultado = 0;
        Connection conexion = null;
        String sentencia_guardada2 = "UPDATE usuarios SET idUsuarios=?,nombre=?,apellidos=?,telefono=?, tipo_de_usuario=? where idUsuarios = ?;";
        try {
            // se registra el libro
            conexion = conexionBI.conector();
            sentencia_preparada = conexion.prepareStatement(sentencia_guardada2);
            sentencia_preparada.setString(1, idusuarios);
            sentencia_preparada.setString(2, nombre);
            sentencia_preparada.setString(3, apellidos);
            sentencia_preparada.setString(4, telefono);
            sentencia_preparada.setString(5, tipousuario);
            sentencia_preparada.setString(6, idusuarios);
            resultado = sentencia_preparada.executeUpdate();
            sentencia_preparada.close();//cierra conexion
            conexion.close();
//            JOptionPane.showMessageDialog(null, "Alumno Modificado");

        } catch (SQLException e) {
            System.out.println("modificarusuarioalumno: " + e);
        }
        return resultado;
    }
    
    public static String busquedaNomEditorial(String correo) {//funciona
        String busqueda_nombre = null;
        Connection conexion = null;
        String editorial = "no existe";
        try {
            conexion = conexionBI.conector();
            //conexion = 
            String sentencia_buscar = ("select idEditorial from editorial where idEditorial = '" + correo + "';");
            sentencia_preparada = (PreparedStatement) conexion.prepareStatement(sentencia_buscar);
            resultado = sentencia_preparada.executeQuery();
            if (resultado.next()) {
                editorial = resultado.getString("idEditorial");
                conexion.close();
            }
            conexion.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        return editorial;
    }
    
    public static String busquedaidUsuario(String correo) {//funciona
        String busqueda_nombre = null;
        Connection conexion = null;
        String editorial = "no existe";
        try {
            conexion = conexionBI.conector();
            //conexion = 
            String sentencia_buscar = ("select idUsuarios from usuarios where idUsuarios = '" + correo + "';");
            sentencia_preparada = (PreparedStatement) conexion.prepareStatement(sentencia_buscar);
            resultado = sentencia_preparada.executeQuery();
            if (resultado.next()) {
                editorial = resultado.getString("idUsuarios");
                conexion.close();
                editorial = "si existe";
            }
            conexion.close();
        } catch (Exception e) {
            System.out.println("buscar usuario" +e);
        }
        return editorial;
    }
    
    public static String busquedaadeudo(String correo) {//funciona
        String busqueda_nombre = null;
        Connection conexion = null;
        String editorial = "no adeudo";
        try {
            conexion = conexionBI.conector();
            //conexion = 
            String sentencia_buscar = ("select Alumnos_Matricula from adeudos where Alumnos_Matricula = " + correo + ";");
            sentencia_preparada = (PreparedStatement) conexion.prepareStatement(sentencia_buscar);
            resultado = sentencia_preparada.executeQuery();
            if (resultado.next()) {
                editorial = resultado.getString("Alumnos_Matricula");
                conexion.close();
                editorial = "adeudo";
            }
            conexion.close();
        } catch (Exception e) {
            System.out.println("buscar adeudo" +e);
        }
        return editorial;
    }
    
    public static String busquedalibrosprestar(String correo) {//funciona
        String busqueda_nombre = null;
        Connection conexion = null;
        String conta = "";
        try {
            conexion = conexionBI.conector();
            //conexion = 
            System.out.println("el correo es "+ correo);
            String sentencia_buscar = ("select count(Libro_idlibro) from prestamo where Alumnos_Matricula = '" + correo + "';");
            sentencia_preparada = (PreparedStatement) conexion.prepareStatement(sentencia_buscar);
            resultado = sentencia_preparada.executeQuery();
            if (resultado.next()) {
                conta = resultado.getString("count(Libro_idlibro)");
                conexion.close();
                System.out.println("conta es "+ conta );
            }
            conexion.close();
        } catch (Exception e) {
            System.out.println("buscar libro presatar" +e);
        }
        return conta;
    }
    public static String busquedanombrealumno(String correo) {//funciona
        String busqueda_nombre = null;
        Connection conexion = null;
        String apellidos = "";
        String nombre = "";
        String completo = "";
        try {
            conexion = conexionBI.conector();
            //conexion = 
            String sentencia_buscar = ("select nombre,apellidos from usuarios where idUSuarios = '" + correo + "';");
            sentencia_preparada = (PreparedStatement) conexion.prepareStatement(sentencia_buscar);
            resultado = sentencia_preparada.executeQuery();
            if (resultado.next()) {
                nombre = resultado.getString("nombre");
                apellidos =resultado.getString("apellidos");
                conexion.close();
                completo = nombre + " " + apellidos;
            }
            conexion.close();
        } catch (Exception e) {
            System.out.println("buscar nombre alumno" +e);
        }
        return completo;
    }
    public static String busquedasemestre(String correo) {//funciona
        String busqueda_nombre = null;
        Connection conexion = null;
        String semestre = "";
        try {
            conexion = conexionBI.conector();
            //conexion = 
            String sentencia_buscar = ("select Semestre from alumnos where Matricula = " + correo + ";");
            sentencia_preparada = (PreparedStatement) conexion.prepareStatement(sentencia_buscar);
            resultado = sentencia_preparada.executeQuery();
            if (resultado.next()) {
                semestre = resultado.getString("Semestre");
                conexion.close();
            }
            conexion.close();
        } catch (Exception e) {
            System.out.println("buscarsemestre" +e);
        }
        return semestre;
    }
    public static String busquedaidlibro(String correo) {//funciona
        String busqueda_nombre = null;
        Connection conexion = null;
        String idlibr = "no existe";
        try {
            conexion = conexionBI.conector();
            //conexion = 
            String sentencia_buscar = ("select idlibro from libro where idlibro = '" + correo + "';");
            sentencia_preparada = (PreparedStatement) conexion.prepareStatement(sentencia_buscar);
            resultado = sentencia_preparada.executeQuery();
            if (resultado.next()) {
                idlibr = resultado.getString("idlibro");
                conexion.close();
            }
            conexion.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        return idlibr;
    }

    public static String busquedamatricula(String correo) {//funciona
        String busqueda_nombre = null;
        Connection conexion = null;
        String matricula = "no existe";
        try {
            conexion = conexionBI.conector();
            //conexion = 
            String sentencia_buscar = ("select matricula from alumnos where matricula = '" + correo + "';");
            sentencia_preparada = (PreparedStatement) conexion.prepareStatement(sentencia_buscar);
            resultado = sentencia_preparada.executeQuery();
            if (resultado.next()) {
                matricula = resultado.getString("matricula");
                conexion.close();
            }
            conexion.close();
        } catch (Exception e) {
            System.out.println("busquedamatricula: "+e);
        }
        return matricula;
    }
    public static String busquedausuariosmatricula(String correo) {//funciona
        String busqueda_nombre = null;
        Connection conexion = null;
        String matricula = "no existe";
        try {
            conexion = conexionBI.conector();
            //conexion = 
            String sentencia_buscar = ("select idUsuarios from usuarios where idUsuarios = '" + correo + "';");
            sentencia_preparada = (PreparedStatement) conexion.prepareStatement(sentencia_buscar);
            resultado = sentencia_preparada.executeQuery();
            if (resultado.next()) {
                matricula = resultado.getString("idUsuarios");
                conexion.close();
                System.out.println("matricula es " +  matricula);
            }
            conexion.close();
        } catch (Exception e) {
            System.out.println("busquedausuarios maticula: "+e);
        }
        return matricula;
    }
    public int guardar_usuarioalumno(String idusuario, String nombre, String apellido, String telefono, String tipousuario) {// funciona
        Connection conexion = null;
        int resultado = 0;
        String sentencia_guardada = "insert into usuarios (idUsuarios, nombre, apellidos, telefono, tipo_de_usuario)values (?,?,?,?,?);";
        try {
            // se registra la editorial
            conexion = conexionBI.conector();
            sentencia_preparada = conexion.prepareStatement(sentencia_guardada);
            sentencia_preparada.setString(1, idusuario);
            sentencia_preparada.setString(2, nombre);
            sentencia_preparada.setString(3, apellido);
            sentencia_preparada.setString(4, telefono);
            sentencia_preparada.setString(5, tipousuario);

            resultado = sentencia_preparada.executeUpdate();
            sentencia_preparada.close();//cierra conexion
            conexion.close();
        } catch (Exception e) {
            System.out.println("guardarusuarioalumno " + e);
        }
        return resultado;
    }
    public int vercandidadlibro(String id) {// funciona
        Connection conexion = null;
        int resultad = 0;
        String sentencia_guardada = "select NoEjem from libro where idlibro = '" + id+ "';";
        try {
            // se registra la editorial
            conexion = conexionBI.conector();
            sentencia_preparada = conexion.prepareStatement(sentencia_guardada);
            resultado = sentencia_preparada.executeQuery();
            if (resultado.next()) {
                resultad = Integer.parseInt(resultado.getString("NoEjem"));
                conexion.close();
            }
            sentencia_preparada.close();//cierra conexion
            conexion.close();
        } catch (Exception e) {
            System.out.println(" ver cantidad de libros " + e);
        }
        return resultad;
    }
    public int guardar_alumno(int matricula, int semestre, String carrera, byte[] imagen) {// funciona
        Connection conexion = null;
        int resultado = 0;
        String sentencia_guardada = "insert into alumnos (Matricula, Semestre,foto, Carrera_idCarrera, Usuarios_idusuarios)values (?,?,?,?,?);";
        try {
            // se registra la editorial
            conexion = conexionBI.conector();
            sentencia_preparada = conexion.prepareStatement(sentencia_guardada);
            sentencia_preparada.setInt(1, matricula);
            sentencia_preparada.setInt(2, semestre);
            sentencia_preparada.setBytes(3, imagen);
            sentencia_preparada.setString(4, carrera);
            sentencia_preparada.setInt(5, matricula);

            resultado = sentencia_preparada.executeUpdate();
            sentencia_preparada.close();//cierra conexion
            conexion.close();
        } catch (Exception e) {
            System.out.println("guardaralumno " + e);
        }
        return resultado;
    }
    public static String busquedaNomEdicion(String correo) {//funciona
        String busqueda_nombre = null;
        Connection conexion = null;
        String edicion = "no existe";
        try {
            conexion = conexionBI.conector();
            //conexion = 
            String sentencia_buscar = ("select idedicion from edicion where idedicion =' "+ correo + "';");
            sentencia_preparada = (PreparedStatement) conexion.prepareStatement(sentencia_buscar);
            resultado = sentencia_preparada.executeQuery();
            if (resultado.next()) {
                edicion = resultado.getString("idedicion");
                conexion.close();
            }
            conexion.close();
        } catch (Exception e) {
            System.out.println("error en busqueda nombre edicion \n " + e);
        }
        return edicion;
    }

    public static String busquedanombre(String correo) {//funciona
        String busqueda_nombre = null;
        Connection conexion = null;
        try {
            conexion = conexionBI.conector();
            //conexion = 
            String sentencia_buscar = ("select nombre, apellidos from usuarios where idUsuarios = '" + correo + "'");
            sentencia_preparada = (PreparedStatement) conexion.prepareStatement(sentencia_buscar);
            resultado = sentencia_preparada.executeQuery();
            if (resultado.next()) {
                String nombre = resultado.getString("nombre");
                String apellidos = resultado.getString("apellidos");
                busqueda_nombre = nombre + " " + apellidos;
                conexion.close();
            }
        } catch (Exception e) {
            System.out.println("error busqueda de nombre: " + e);
        }
        return busqueda_nombre;
    }

    public int guardarusuarios(String idusuario, String nombre, String apellido,String contrasena, String telefono, String tipousuario) {// funciona
        Connection conexion = null;
        int resultado = 0;
        String sentencia_guardada = "insert into usuarios (idUsuarios, nombre, apellidos,contrasena, telefono, tipo_de_usuario)values (?,?,?,?,?,?);";
        try {
            // se registra la editorial
            conexion = conexionBI.conector();
            sentencia_preparada = conexion.prepareStatement(sentencia_guardada);
            sentencia_preparada.setString(1, idusuario);
            sentencia_preparada.setString(2, nombre);
            sentencia_preparada.setString(3, apellido);
            sentencia_preparada.setString(4, contrasena);
            sentencia_preparada.setString(5, telefono);
            sentencia_preparada.setString(6, tipousuario);

            resultado = sentencia_preparada.executeUpdate();
            sentencia_preparada.close();//cierra conexion
            conexion.close();
        } catch (Exception e) {
            System.out.println("guardarUsuarios " + e);
        }
        return resultado;
    }
    
    public int modificarusuarios(String idusuario, String nombre, String apellido,String contrasena, String telefono, String tipousuario) {// funciona
        Connection conexion = null;
        int resultado = 0;
        String sentencia_guardada = "UPDATE usuarios SET idUsuarios =?, set nombre =?, set apellidos = ?, set contrasena = ?, set telefono = ?, set tipo_de_usuario = ?where idUsuarios = '"+ idusuario +"'; ";        
        try {
            // se registra la editorial
            conexion = conexionBI.conector();
            sentencia_preparada = conexion.prepareStatement(sentencia_guardada);
            sentencia_preparada.setString(1, idusuario);
            sentencia_preparada.setString(2, nombre);
            sentencia_preparada.setString(3, apellido);
            sentencia_preparada.setString(4, contrasena);
            sentencia_preparada.setString(5, telefono);
            sentencia_preparada.setString(6, tipousuario);
            resultado = sentencia_preparada.executeUpdate();
            sentencia_preparada.close();//cierra conexion
            conexion.close();
        } catch (Exception e) {
            System.out.println("modificarUsuarios " + e);
        }
        return resultado;
    }
    public int modificarlibro2(String idlibr, int num) {// funciona
        Connection conexion = null;
        int resultado = 0;
        String sentencia_guardada = "UPDATE libro SET NoEjem = ? where idlibro = '"+ idlibr +"'; ";        
        try {
            // se registra la editorial
            conexion = conexionBI.conector();
            sentencia_preparada = conexion.prepareStatement(sentencia_guardada);
            sentencia_preparada.setInt(1, num);
            resultado = sentencia_preparada.executeUpdate();
            sentencia_preparada.close();//cierra conexion
            conexion.close();
        } catch (Exception e) {
            System.out.println("modificarUsuarios " + e);
        }
        return resultado;
    }
    public int eliminarpretsamo(String matricula, String idlibr) {// funciona
        Connection conexion = null;
        int resultad = 0;
        String sentencia_guardada = "delete from prestamo where Alumnos_Matricula = ? ;";        
        try {
            // se registra la editorial
            conexion = conexionBI.conector();
            sentencia_preparada = conexion.prepareStatement(sentencia_guardada);
            //if(resultado.next()){
            sentencia_preparada.setString(1, matricula);
            //sentencia_preparada.setString(2, idlibr);
            resultad = sentencia_preparada.executeUpdate();
            
            sentencia_preparada.close();//cierra conexion
            conexion.close();
        } catch (Exception e) {
            System.out.println("eliminar prestamo " + e);
        }
        return resultad;
    }
    public static String Usuariosregistrados(String usuario, String contrasena) {//funciona
        String Busqueda_usuario = null;
        Connection conexion = null;
        try {
            conexion = conexionBI.conector();
            String sentencia_buscar_usuarios = "select nombre, apellidos from Usuarios where idusuarios = '" + usuario + "' && contrasena = '" + contrasena + "'";
            sentencia_preparada = (PreparedStatement) conexion.prepareStatement(sentencia_buscar_usuarios);
            resultado = sentencia_preparada.executeQuery();
            if (resultado.next()) {
                Busqueda_usuario = "usuario encontrado";
            } else {
                Busqueda_usuario = "usuario no encontrado";
            }
            conexion.close();
        } catch (Exception e) {
            System.out.println("error al usuarioregistrado: " + e);
        }
        return Busqueda_usuario;
    }

    /*    
    public boolean registrar(Usuarios usr) {
        java.sql.PreparedStatement ps = null;
        Connection con = conexionBD.conectar1();;

        String sql = "INSERT INTO usuarios (usuario, password, nombre, correo, idTipo) VALUES(?,?,?,?,?)";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, usr.getUsuario());
            ps.setString(2, usr.getPassword());
            ps.setString(3, usr.getNombre());
            ps.setString(4, usr.getEmail());
            ps.setInt(5, usr.getIdTipo());
            ps.execute();
            return true;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.toString());
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e.toString());
            }
        }
    }

    public boolean login(Usuarios usr) {
        java.sql.PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = getConexion();

        String sql = "SELECT id, usuario, password, nombre, idTipo FROM usuarios WHERE usuario = ? LIMIT 1";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, usr.getUsuario());
            rs = ps.executeQuery();

            if (rs.next()) {
                if (usr.getPassword().equals(rs.getString(3))) {
                    usr.setId(rs.getInt(1));
                    usr.setNombre(rs.getString(4));
                    usr.setIdTipo(rs.getInt(5));
                    return true;
                } else {
                    return false;
                }
            }

            return false;
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.toString());
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e.toString());
            }
        }
    }
     */
    public int existeUsuario(String usuario) {
        java.sql.PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = conexionBI.conector();

        String sql = "SELECT count(id) FROM usuarios WHERE usuario = ?";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, usuario);
            rs = ps.executeQuery();

            if (rs.next()) {
                return rs.getInt(1);
            }

            return 1;

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, e.toString());
            return 1;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e.toString());
            }
        }
    }
    /*
    public static String guardar() {
        com.mysql.jdbc.Connection con = null;

        try {

            con = getConection();
            ps = con.prepareStatement("INSERT INTO persona (clave, nombre, domicilio, telefono, correo_electronico, fecha_nacimiento, genero) VALUES(?,?,?,?,?,?,?) ");
            ps.setString(1, txtClave.getText());
            ps.setString(2, txtNombre.getText());
            ps.setString(3, txtDomicilio.getText());
            ps.setString(4, txtTelefono.getText());
            ps.setString(5, txtEmail.getText());
            ps.setDate(6, Date.valueOf(txtFecha.getText()));
            ps.setString(7, cbxGenero.getSelectedItem().toString());

            int res = ps.executeUpdate();

            if (res > 0) {
                JOptionPane.showMessageDialog(null, "Persona Guardada");
                limpiarCajas();
            } else {
                JOptionPane.showMessageDialog(null, "Error al Guardar persona");
                limpiarCajas();
            }

            con.close();

        } catch (Exception e) {
            System.err.println(e);
        }
    }*/

}

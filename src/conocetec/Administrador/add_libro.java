package conocetec.Administrador;

//import com.mysql.jdbc.ResultSetMetaData; // tiene un error cuando lo abri despues de un tiempo
// nota: funcionaba cuando lo entrege
import conocetec.metodossql.conexionBI;
import conocetec.metodossql.metodos;
import static conocetec.metodossql.metodos.resultado;
import static conocetec.metodossql.metodos.sentencia_preparada;
import java.awt.Color;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class add_libro extends javax.swing.JFrame {
    
    
    conexionBI cc = new conexionBI();
    Connection cn = cc.conector();
    
    public String seleccionadoeditorial = "Seleccionar";
    public String seleccionadoedicion = "Seleccionar";
    public String seleccionadoarea = "Seleccionar";

    public add_libro() {
        initComponents();
        iniciar();
    }
    void iniciar(){
        bottbuscar.setEnabled(false);
        metodos com1 = new metodos();
        metodos com2 = new metodos();
        metodos com3 = new metodos();
    setLocationRelativeTo(null); // localiza la panalla en el centro :V
        this.getContentPane().setBackground(new Color(150, 201, 255)); // color de fondo
//       this.jPanel1.setBackground(new Color(150, 201, 255)); // color al panel
        com1.comboboxeditorial(BoxEdi);
        // com2.comboboxedicion(BoxEdicion);
        com3.comboboxareas(BoxArea);
        mostrardatos();
        
        
        ImageIcon img1 = new  ImageIcon(getClass().getResource("/imagenes/flecha-curva-izquierda.png"));
        ImageIcon icon = new ImageIcon(img1.getImage().getScaledInstance((this.bottatras.getWidth())/4, this.bottatras.getHeight(), 1));
        this.bottatras.setIcon(icon);
        
        ImageIcon img2 = new  ImageIcon(getClass().getResource("/imagenes/guardar.png"));
        ImageIcon icon2 = new ImageIcon(img2.getImage().getScaledInstance((this.bottguardar.getWidth())/4, this.bottguardar.getHeight(), 1));
        this.bottguardar.setIcon(icon2);
        
        ImageIcon img3 = new  ImageIcon(getClass().getResource("/imagenes/lupa.png"));
        ImageIcon icon3 = new ImageIcon(img3.getImage().getScaledInstance((this.bottbuscar.getWidth())/4, this.bottbuscar.getHeight(), 1));
        this.bottbuscar.setIcon(icon3);
        
        ImageIcon img4 = new  ImageIcon(getClass().getResource("/imagenes/limpiar.png"));
        ImageIcon icon4 = new ImageIcon(img4.getImage().getScaledInstance((this.bottlimpiar.getWidth())/4, this.bottlimpiar.getHeight(), 1));
        this.bottlimpiar.setIcon(icon4);
        
        ImageIcon img5 = new  ImageIcon(getClass().getResource("/imagenes/basura.png"));
        ImageIcon icon5 = new ImageIcon(img5.getImage().getScaledInstance((this.botteliminar.getWidth())/4, this.botteliminar.getHeight(), 1));
        this.botteliminar.setIcon(icon5);
}
    
    void mostrardatos() {
        DefaultTableModel modelo = new DefaultTableModel();
        modelo.addColumn("id de Libro");
        modelo.addColumn("Titulo");
        modelo.addColumn("Año");
        modelo.addColumn("Estante");
        modelo.addColumn("Autor");
        modelo.addColumn("N° de Ejemplares");
        modelo.addColumn("Editorial");
        modelo.addColumn("Carrera");
        jtlibros.setModel(modelo);
        String[] datos = new String[10];
        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery("select * from libro;");
            while (rs.next()) {
                datos[0] = rs.getString(1);
                datos[1] = rs.getString(2);
                datos[2] = rs.getString(3);
                datos[3] = rs.getString(4);
                datos[4] = rs.getString(5);
                datos[5] = rs.getString(6);
                datos[6] = rs.getString(7);
                datos[7] = rs.getString(8);

                modelo.addRow(datos);
            }
            jtlibros.setModel(modelo);
        } catch (SQLException ex) {
            Logger.getLogger(add_libro.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        bottatras = new javax.swing.JButton();
        bottguardar = new javax.swing.JButton();
        bottmodificar = new javax.swing.JButton();
        bottbuscar = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        bottlimpiar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtlibros = new javax.swing.JTable();
        txtbuscar = new javax.swing.JTextField();
        lamefondo = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtidlibro = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        txttitulo = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtano = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtnoingre = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtAutor = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        txtestante = new javax.swing.JTextField();
        BoxEdi = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        BoxArea = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        txteditorial = new javax.swing.JTextField();
        botteliminar = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();

        jMenuItem1.setText("Modificar");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(jMenuItem1);

        jMenuItem2.setText("Eliminar");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(jMenuItem2);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(950, 720));
        setMinimumSize(new java.awt.Dimension(950, 720));
        setUndecorated(true);
        setPreferredSize(new java.awt.Dimension(950, 720));
        getContentPane().setLayout(null);

        bottatras.setText("atras");
        bottatras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bottatrasActionPerformed(evt);
            }
        });
        getContentPane().add(bottatras);
        bottatras.setBounds(18, 15, 95, 23);

        bottguardar.setText("Guardar");
        bottguardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bottguardarActionPerformed(evt);
            }
        });
        getContentPane().add(bottguardar);
        bottguardar.setBounds(41, 279, 110, 23);

        bottmodificar.setText("Modificar");
        bottmodificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bottmodificarActionPerformed(evt);
            }
        });
        getContentPane().add(bottmodificar);
        bottmodificar.setBounds(300, 280, 100, 23);

        bottbuscar.setText("Buscar");
        bottbuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bottbuscarActionPerformed(evt);
            }
        });
        getContentPane().add(bottbuscar);
        bottbuscar.setBounds(206, 330, 140, 23);

        jLabel7.setText("USUARIO: Administrador");
        getContentPane().add(jLabel7);
        jLabel7.setBounds(40, 640, 170, 20);

        bottlimpiar.setText("Limpiar");
        bottlimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bottlimpiarActionPerformed(evt);
            }
        });
        getContentPane().add(bottlimpiar);
        bottlimpiar.setBounds(530, 280, 140, 23);

        jtlibros.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jtlibros.setComponentPopupMenu(jPopupMenu1);
        jScrollPane1.setViewportView(jtlibros);

        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(20, 440, 866, 170);

        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });
        getContentPane().add(txtbuscar);
        txtbuscar.setBounds(41, 321, 147, 30);
        getContentPane().add(lamefondo);
        lamefondo.setBounds(517, 330, 56, 23);

        jLabel11.setFont(new java.awt.Font("Trebuchet MS", 2, 14)); // NOI18N
        jLabel11.setText("id Libro");
        getContentPane().add(jLabel11);
        jLabel11.setBounds(90, 70, 49, 18);

        txtidlibro.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtidlibroKeyTyped(evt);
            }
        });
        getContentPane().add(txtidlibro);
        txtidlibro.setBounds(180, 60, 140, 30);

        jLabel1.setFont(new java.awt.Font("Trebuchet MS", 2, 14)); // NOI18N
        jLabel1.setText("Titulo:");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(90, 110, 43, 18);

        txttitulo.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        txttitulo.setPreferredSize(new java.awt.Dimension(100, 20));
        txttitulo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txttituloActionPerformed(evt);
            }
        });
        txttitulo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txttituloKeyTyped(evt);
            }
        });
        getContentPane().add(txttitulo);
        txttitulo.setBounds(180, 110, 140, 27);

        jLabel2.setFont(new java.awt.Font("Trebuchet MS", 2, 14)); // NOI18N
        jLabel2.setText("Año:");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(90, 160, 70, 30);

        txtano.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        txtano.setPreferredSize(new java.awt.Dimension(100, 20));
        txtano.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtanoActionPerformed(evt);
            }
        });
        txtano.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtanoKeyTyped(evt);
            }
        });
        getContentPane().add(txtano);
        txtano.setBounds(180, 160, 140, 27);

        jLabel3.setFont(new java.awt.Font("Trebuchet MS", 2, 14)); // NOI18N
        jLabel3.setText("No° de ingreso: ");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(30, 220, 104, 18);

        txtnoingre.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        txtnoingre.setPreferredSize(new java.awt.Dimension(100, 20));
        txtnoingre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnoingreActionPerformed(evt);
            }
        });
        txtnoingre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtnoingreKeyTyped(evt);
            }
        });
        getContentPane().add(txtnoingre);
        txtnoingre.setBounds(180, 210, 140, 31);

        jLabel4.setFont(new java.awt.Font("Trebuchet MS", 2, 14)); // NOI18N
        jLabel4.setText("Autor:");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(430, 70, 42, 18);

        txtAutor.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        txtAutor.setPreferredSize(new java.awt.Dimension(100, 20));
        txtAutor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAutorActionPerformed(evt);
            }
        });
        txtAutor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtAutorKeyTyped(evt);
            }
        });
        getContentPane().add(txtAutor);
        txtAutor.setBounds(490, 60, 140, 27);

        jLabel13.setFont(new java.awt.Font("Trebuchet MS", 2, 14)); // NOI18N
        jLabel13.setText("estante:");
        getContentPane().add(jLabel13);
        jLabel13.setBounds(420, 120, 54, 18);

        txtestante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtestanteActionPerformed(evt);
            }
        });
        getContentPane().add(txtestante);
        txtestante.setBounds(490, 110, 140, 30);

        BoxEdi.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccionar" }));
        BoxEdi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BoxEdiActionPerformed(evt);
            }
        });
        getContentPane().add(BoxEdi);
        BoxEdi.setBounds(490, 160, 140, 20);

        jLabel5.setFont(new java.awt.Font("Trebuchet MS", 2, 14)); // NOI18N
        jLabel5.setText("Editorial:");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(420, 160, 60, 18);

        jLabel8.setFont(new java.awt.Font("Trebuchet MS", 2, 14)); // NOI18N
        jLabel8.setText("Area:");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(440, 210, 35, 18);

        BoxArea.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccionar" }));
        BoxArea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BoxAreaActionPerformed(evt);
            }
        });
        getContentPane().add(BoxArea);
        BoxArea.setBounds(490, 210, 140, 20);

        jLabel9.setFont(new java.awt.Font("Trebuchet MS", 2, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("ó");
        getContentPane().add(jLabel9);
        jLabel9.setBounds(670, 160, 10, 18);

        txteditorial.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        txteditorial.setPreferredSize(new java.awt.Dimension(100, 20));
        txteditorial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txteditorialActionPerformed(evt);
            }
        });
        txteditorial.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txteditorialKeyTyped(evt);
            }
        });
        getContentPane().add(txteditorial);
        txteditorial.setBounds(740, 160, 140, 27);

        botteliminar.setText("Eliminar");
        botteliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botteliminarActionPerformed(evt);
            }
        });
        getContentPane().add(botteliminar);
        botteliminar.setBounds(733, 280, 130, 23);

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/estante.png"))); // NOI18N
        getContentPane().add(jLabel6);
        jLabel6.setBounds(220, 20, 570, 520);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtanoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtanoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtanoActionPerformed

    private void txtanoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtanoKeyTyped
        char car = evt.getKeyChar(); // usa el codigo ascci
        if (txtano.getText().length() >= 4) {
            evt.consume(); // establece a solo 4 numero
        }
        if ((car < '0' || car > '9')) { // solo acepta numeros
            evt.consume(); // consume si no son numeros
        }
    }//GEN-LAST:event_txtanoKeyTyped

    private void txttituloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txttituloActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txttituloActionPerformed

    private void txttituloKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttituloKeyTyped
        char car = evt.getKeyChar(); // usa el codigo ascci
        if (txttitulo.getText().length() >= 70) {
            evt.consume(); // establece a solo 70 letras
        }
    }//GEN-LAST:event_txttituloKeyTyped

    private void txtnoingreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnoingreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnoingreActionPerformed

    private void txtnoingreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnoingreKeyTyped
        // TODO add your handling code here:
        char car = evt.getKeyChar(); // usa el codigo ascci
        if (txtnoingre.getText().length() >= 2) {
            evt.consume(); // establece a solo 2 numero
        }
        if ((car < '0' || car > '9')) { // solo acepta numeros
            evt.consume(); // consume si no son numeros
        }
    }//GEN-LAST:event_txtnoingreKeyTyped

    private void txtAutorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAutorActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtAutorActionPerformed

    private void txtAutorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAutorKeyTyped
        // TODO add your handling code here:
        char car = evt.getKeyChar(); // usa el codigo ascci
        if (txtAutor.getText().length() >= 50) {
            evt.consume(); // establece a solo 50 letras
        }
        if ((car < 'a' || car > 'z') && (car < 'A' || car > 'Z') && (car == '.')) { // solo acepta letras y puntos 
            evt.consume(); // consume si no son letras
        }
    }//GEN-LAST:event_txtAutorKeyTyped

    private void txteditorialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txteditorialActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txteditorialActionPerformed

    private void txteditorialKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txteditorialKeyTyped
        // TODO add your handling code here:
        char car = evt.getKeyChar(); // usa el codigo ascci
        if (txteditorial.getText().length() >= 30) {
            evt.consume(); // establece a solo 30 letras
        }
    }//GEN-LAST:event_txteditorialKeyTyped

    private void bottguardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bottguardarActionPerformed
        // TODO add your handling code here:
        botteliminar.setEnabled(true);
        bottguardar.setEnabled(true);
        bottlimpiar.setEnabled(true);
        bottbuscar.setEnabled(true);
        bottmodificar.setEnabled(true);
        bottatras.setEnabled(true);
        
        metodos met = new metodos();
        boolean bol = true;
        String edi = met.busquedaNomEditorial(txteditorial.getText());
        String edi2 = edi;
        int i = BoxEdi.getSelectedIndex();
        //int j = BoxEdicion.getSelectedIndex();
        System.out.println("i es: " + i);

        if (met.busquedaidlibro(txtidlibro.getText()) != "no existe") {
            JOptionPane.showMessageDialog(null, "el id del libro ya esta registrado");
            bol = false;
        }

        if ((i != 0 && txteditorial.getText().length() == 0) || (i == 0 && txteditorial.getText().length() > 0)) {
            if (seleccionadoeditorial != "Seleccionar") {
                edi = seleccionadoeditorial;
                System.out.println("edi en ComboBox");
            }
            if (edi2.equalsIgnoreCase("no existe")) {
                met.guardar_editorial(txteditorial.getText());
                edi = txteditorial.getText();
            }
        } else {
            JOptionPane.showMessageDialog(null, "verifique Editorial");
            bol = false;
        }

        /* String edicio = met.busquedaNomEdicion(txtedicion.getText());
        System.out.println(" variable edicio: " + edicio);
        if ((j != 0 && txtedicion.getText().length() == 0) || (j == 0 && txtedicion.getText().length() > 0)) {
            if (seleccionadoedicion != "Seleccionar") {
                edicio = seleccionadoedicion;
            }
            if (edicio.equalsIgnoreCase("no existe")) {
                met.guardar_edicion(txtedicion.getText());
                edicio = (txtedicion.getText());// no asigna el valor
                //System.out.println(" variable edicio2: " + edicio);
                System.out.println("el txtedicion es: " + txtedicion.getText());
            }
        } else {
            JOptionPane.showMessageDialog(null, "verifique Edicion");
            bol = false;
        }*/
        String are = "";
        if (seleccionadoarea != "Seleccionar" && edi != "Seleccionar" && txtAutor.getText().length() > 0 && txtano.getText().length() > 0 && txtestante.getText().length() > 0 && txtidlibro.getText().length() > 0 && txtnoingre.getText().length() > 0 && txttitulo.getText().length() > 0 && bol == true) {
            are = seleccionadoarea;
            System.out.println(seleccionadoarea);
            met.guardarLibro(txtidlibro.getText(), txttitulo.getText(), txtano.getText(), txtAutor.getText(), txtnoingre.getText(), txtestante.getText(), edi, are);
            limpiarCajas();
            mostrardatos();
        } else {
            JOptionPane.showMessageDialog(null, "verifique los datos");
        }
        iniciar();

    }//GEN-LAST:event_bottguardarActionPerformed

    private void txtidlibroKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtidlibroKeyTyped
        // TODO add your handling code here:
        char car = evt.getKeyChar(); // usa el codigo ascci
        if (txtidlibro.getText().length() >= 11) {
            evt.consume(); // establece a solo 11 numero
        }
        if ((car < '0' || car > '9')) { // solo acepta numeros
            evt.consume(); // consume si no son numeros
        }
    }//GEN-LAST:event_txtidlibroKeyTyped

    private void BoxAreaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BoxAreaActionPerformed
        // TODO add your handling code here:
        Object obj = BoxArea.getSelectedItem();
        seleccionadoarea = String.valueOf(obj);
    }//GEN-LAST:event_BoxAreaActionPerformed

    private void bottatrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bottatrasActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_bottatrasActionPerformed

    private void BoxEdiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BoxEdiActionPerformed
        // TODO add your handling code here:
        Object obj = BoxEdi.getSelectedItem();
        seleccionadoeditorial = String.valueOf(obj);
        //this.txteditorial.setText("");

    }//GEN-LAST:event_BoxEdiActionPerformed

    private void bottmodificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bottmodificarActionPerformed
        // TODO add your handling code here:
        botteliminar.setEnabled(true);
        bottguardar.setEnabled(true);
        bottlimpiar.setEnabled(true);
        bottbuscar.setEnabled(true);
        bottmodificar.setEnabled(true);
        bottatras.setEnabled(true);
        
        metodos met = new metodos();
        boolean bol = true;
        //if(met.busquedaidlibro(txtidlibro.getText()) != "no existe"){
        //    JOptionPane.showMessageDialog(null, "el id del libro ya esta registrado");
        //    bol = false;
        //}
        String edi = met.busquedaNomEditorial(txteditorial.getText());
        String edi2 = edi;
        int i = BoxEdi.getSelectedIndex();

        if ((i != 0 && txteditorial.getText().length() == 0) || (i == 0 && txteditorial.getText().length() > 0)) {
            if (seleccionadoeditorial != "Seleccionar") {
                edi = seleccionadoeditorial;
            }
            if (edi2.equalsIgnoreCase("no existe")) {
                met.guardar_editorial(txteditorial.getText());
                edi = txteditorial.getText();
            }
        } else {
            JOptionPane.showMessageDialog(null, "verifique Editorial");
            bol = false;
        }

        String are = "";
        if (seleccionadoarea != "Seleccionar" && edi != "Seleccionar" && txtAutor != null && txtano
                != null && txtestante != null && txtidlibro != null && txtnoingre != null && txttitulo != null && bol == true) {
            are = seleccionadoarea;
            System.out.println(seleccionadoarea);
            met.modificarLibro(txtidlibro.getText(), txttitulo.getText(), txtano.getText(), txtAutor.getText(), txtnoingre.getText(), txtestante.getText(), edi, seleccionadoarea);
            limpiarCajas();
            mostrardatos();
        } else {
            JOptionPane.showMessageDialog(null, "verifique los datos");
        }


    }//GEN-LAST:event_bottmodificarActionPerformed

    private void bottlimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bottlimpiarActionPerformed
        // TODO add your handling code here:
        limpiarCajas();
        botteliminar.setEnabled(true);
        bottguardar.setEnabled(true);
        bottlimpiar.setEnabled(true);
        bottbuscar.setEnabled(false);
        bottmodificar.setEnabled(true);
        bottatras.setEnabled(true);
    }//GEN-LAST:event_bottlimpiarActionPerformed

    private void bottbuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bottbuscarActionPerformed
        // TODO add your handling code here:
        botteliminar.setEnabled(false);
        bottguardar.setEnabled(false);
        bottlimpiar.setEnabled(true);
        bottbuscar.setEnabled(false);
        bottmodificar.setEnabled(true);
        bottatras.setEnabled(true);
        
        String cadu = "";
        String pre = "";
//        char arrcadu[]=cadu.toCharArray();
        if (txtbuscar.getText().length() > 0) {
        Connection conexion = null;
        //int resultado = 0;
        try {
            conexion = conexionBI.conector();
        String sentencia_buscar = "select * from libro where idlibro = '"+ txtbuscar.getText() + "';";
            sentencia_preparada = (PreparedStatement) conexion.prepareStatement(sentencia_buscar);
            resultado = sentencia_preparada.executeQuery();
            if (resultado.next()) {
                txtidlibro.setText(resultado.getString("idlibro"));
                txttitulo.setText(resultado.getString("Titulo"));
                txtano.setText(resultado.getString("Ano"));
                txtestante.setText(resultado.getString("Estante"));
                txtAutor.setText(resultado.getString("Autor"));
                txtnoingre.setText(resultado.getString("NoEjem"));
                BoxEdi.setSelectedItem(resultado.getString("Editorial_idEditorial"));
                BoxArea.setSelectedItem(resultado.getString("Carrera_idCarrera"));
                //conexion.close();
            }
            conexion.close();
        } catch (Exception e) {
            System.out.println("busqueda con id: " + e);
        }
        
        }
        
        
    }//GEN-LAST:event_bottbuscarActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
                // TODO add your handling code here:
                
        //codigo eliminar
        botteliminar.setEnabled(true);
        bottguardar.setEnabled(true);
        bottlimpiar.setEnabled(true);
        bottbuscar.setEnabled(true);
        bottmodificar.setEnabled(true);
        bottatras.setEnabled(true);
                
        metodos met = new metodos();
        
        if(met.eliminarlibro(txtidlibro.getText()) > 0){
            JOptionPane.showInputDialog(null, "libro eliminado");
            limpiarCajas();
        }else{
            JOptionPane.showInputDialog(null, "libro no encontrado ");
        }

    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
        botteliminar.setEnabled(false);
        bottguardar.setEnabled(false);
        bottlimpiar.setEnabled(true);
        bottbuscar.setEnabled(false);
        bottmodificar.setEnabled(true);
        bottatras.setEnabled(true);
        
        int fila = jtlibros.getSelectedRow();
        String cadu = "";
        String pre = "";
//        char arrcadu[]=cadu.toCharArray();
        if (fila >= 0) {
            txtidlibro.setText(jtlibros.getValueAt(fila, 0).toString());
            txttitulo.setText(jtlibros.getValueAt(fila, 1).toString());
            txtano.setText(jtlibros.getValueAt(fila, 2).toString());
            txtestante.setText(jtlibros.getValueAt(fila, 3).toString());
            txtAutor.setText(jtlibros.getValueAt(fila, 4).toString());
            txtnoingre.setText(jtlibros.getValueAt(fila, 5).toString());
            BoxEdi.setSelectedItem(jtlibros.getValueAt(fila, 6).toString());
            BoxArea.setSelectedItem(jtlibros.getValueAt(fila, 7).toString());

        } else {
        }
        
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void botteliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botteliminarActionPerformed
        // TODO add your handling code here:
        botteliminar.setEnabled(true);
        bottguardar.setEnabled(true);
        bottlimpiar.setEnabled(true);
        bottbuscar.setEnabled(true);
        bottmodificar.setEnabled(true);
        bottatras.setEnabled(true);
                
        metodos met = new metodos();
        
        if(met.eliminarlibro(txtidlibro.getText()) > 0){
            JOptionPane.showInputDialog(null, "libro eliminado");
            limpiarCajas();
        }else{
            JOptionPane.showInputDialog(null, "libro no encontrado ");
        }
    }//GEN-LAST:event_botteliminarActionPerformed

    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
        // TODO add your handling code here:
            if (!txtbuscar.getText().isEmpty()) {//si esta lleno
            bottbuscar.setEnabled(true);
            bottmodificar.setEnabled(false);
            //Guardar.setEnabled(false);
            bottlimpiar.setEnabled(true);
        } else {
            bottbuscar.setEnabled(false);
        }

    }//GEN-LAST:event_txtbuscarKeyReleased

    private void txtestanteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtestanteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtestanteActionPerformed

    
    private void limpiarCajas(){
     
     txtAutor.setText(null);
     txtano.setText(null);
     txteditorial.setText(null);
     txtestante.setText(null);
     txtidlibro.setText(null);
     txtnoingre.setText(null);
     txttitulo.setText(null);
     BoxArea.setSelectedIndex(0);
     BoxEdi.setSelectedIndex(0);
     
 }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(add_libro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(add_libro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(add_libro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(add_libro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new add_libro().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> BoxArea;
    private javax.swing.JComboBox<String> BoxEdi;
    private javax.swing.JButton bottatras;
    private javax.swing.JButton bottbuscar;
    private javax.swing.JButton botteliminar;
    private javax.swing.JButton bottguardar;
    private javax.swing.JButton bottlimpiar;
    private javax.swing.JButton bottmodificar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jtlibros;
    private javax.swing.JLabel lamefondo;
    private javax.swing.JTextField txtAutor;
    private javax.swing.JTextField txtano;
    private javax.swing.JTextField txtbuscar;
    private javax.swing.JTextField txteditorial;
    private javax.swing.JTextField txtestante;
    private javax.swing.JTextField txtidlibro;
    private javax.swing.JTextField txtnoingre;
    private javax.swing.JTextField txttitulo;
    // End of variables declaration//GEN-END:variables
}

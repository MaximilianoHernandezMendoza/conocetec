package conocetec.Administrador;

import conocetec.metodossql.conexionBI;
import static conocetec.metodossql.metodos.resultado;
import static conocetec.metodossql.metodos.sentencia_preparada;
import java.awt.Color;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.swing.JOptionPane;

public class buscarlibroparaprestar extends javax.swing.JFrame {

    String matricu = "";

    public buscarlibroparaprestar() {
        initComponents();
        setLocationRelativeTo(null); // localiza la panalla en el centro :V
        bottprestar.setEnabled(false);
        this.getContentPane().setBackground(new Color(150, 201, 255)); // color de fondo

    }

    public void entramatri(String matri, int cu) {
        matricu = matri;
        if(cu == 1){
            txtcod2.setEnabled(false);
            jLabel2.setEnabled(false);
            jPanel2.setEnabled(false);
        }
        if(cu == 2){
            txtcod2.setEnabled(true);
            jLabel2.setEnabled(true);
            jPanel2.setEnabled(true);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtcod1 = new javax.swing.JTextField();
        txtcod2 = new javax.swing.JTextField();
        jToggleButton1 = new javax.swing.JToggleButton();
        bottbuscar = new javax.swing.JToggleButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtnombre1 = new javax.swing.JTextField();
        txtestante1 = new javax.swing.JTextField();
        txtejemplares1 = new javax.swing.JTextField();
        label1 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtcod11 = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtnombre2 = new javax.swing.JTextField();
        txtestante2 = new javax.swing.JTextField();
        txtejemplares2 = new javax.swing.JTextField();
        label2 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txtcod22 = new javax.swing.JTextField();
        bottprestar = new javax.swing.JToggleButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jLabel1.setText("Libro1:");

        jLabel2.setText("Libro2:");

        txtcod1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtcod1KeyTyped(evt);
            }
        });

        txtcod2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtcod2KeyTyped(evt);
            }
        });

        jToggleButton1.setText("atras");
        jToggleButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton1ActionPerformed(evt);
            }
        });

        bottbuscar.setText("Buscar");
        bottbuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bottbuscarActionPerformed(evt);
            }
        });

        jLabel3.setText("Libro 1:");

        jLabel4.setText("Nombre:");

        jLabel5.setText("Estante:");

        jLabel6.setText("N° de Ejemplares:");

        txtnombre1.setEditable(false);
        txtnombre1.setEnabled(false);

        txtestante1.setEditable(false);
        txtestante1.setEnabled(false);

        txtejemplares1.setEditable(false);
        txtejemplares1.setEnabled(false);
        txtejemplares1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtejemplares1ActionPerformed(evt);
            }
        });

        label1.setText("--");

        jLabel11.setText("Codigo:");

        txtcod11.setEditable(false);
        txtcod11.setEnabled(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(23, 23, 23)
                        .addComponent(jLabel11))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5))))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtejemplares1)
                    .addComponent(txtestante1)
                    .addComponent(txtnombre1)
                    .addComponent(txtcod11, javax.swing.GroupLayout.DEFAULT_SIZE, 209, Short.MAX_VALUE))
                .addGap(126, 126, 126)
                .addComponent(label1)
                .addContainerGap(281, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel11)
                            .addComponent(txtcod11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtnombre1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(label1))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtestante1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addComponent(txtejemplares1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 38, Short.MAX_VALUE))
        );

        jLabel8.setText("Nombre:");

        jLabel7.setText("Libro 2:");

        jLabel9.setText("Estante:");

        jLabel10.setText("Nº de Ejemplares:");

        txtnombre2.setEditable(false);
        txtnombre2.setEnabled(false);

        txtestante2.setEditable(false);
        txtestante2.setEnabled(false);

        txtejemplares2.setEditable(false);
        txtejemplares2.setEnabled(false);

        label2.setText("--");

        jLabel12.setText("Codigo:");

        txtcod22.setEditable(false);
        txtcod22.setEnabled(false);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(26, 26, 26)
                        .addComponent(jLabel12))
                    .addComponent(jLabel10)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel9)
                        .addComponent(jLabel8)))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtnombre2)
                    .addComponent(txtestante2)
                    .addComponent(txtejemplares2)
                    .addComponent(txtcod22, javax.swing.GroupLayout.DEFAULT_SIZE, 203, Short.MAX_VALUE))
                .addGap(130, 130, 130)
                .addComponent(label2)
                .addContainerGap(280, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12)
                            .addComponent(txtcod22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtnombre2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(label2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(txtestante2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(txtejemplares2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(31, Short.MAX_VALUE))
        );

        bottprestar.setText("Prestar");
        bottprestar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bottprestarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jToggleButton1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(77, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(bottbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(18, 18, 18)
                                .addComponent(txtcod1, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel2)
                        .addGap(46, 46, 46)
                        .addComponent(txtcod2, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(248, 248, 248))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(bottprestar)
                .addGap(102, 102, 102))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jToggleButton1)
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtcod1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(txtcod2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addComponent(bottbuscar)
                .addGap(18, 18, 18)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(bottprestar)
                .addContainerGap(33, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jToggleButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton1ActionPerformed
        prestarlibro pre = new prestarlibro();
        pre.buscaralumno(matricu);
        pre.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jToggleButton1ActionPerformed

    private void txtejemplares1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtejemplares1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtejemplares1ActionPerformed

    private void txtcod1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcod1KeyTyped
        // TODO add your handling code here:
        char car = evt.getKeyChar(); // usa el codigo ascci
        if (txtcod1.getText().length() >= 8) {
            evt.consume(); // establece a solo 8 numero
        }
        if ((car < '0' || car > '9')) { // solo acepta numeros
            evt.consume(); // consume si no son numeros
        }

    }//GEN-LAST:event_txtcod1KeyTyped

    private void txtcod2KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcod2KeyTyped
        // TODO add your handling code here:
        char car = evt.getKeyChar(); // usa el codigo ascci
        if (txtcod2.getText().length() >= 8) {
            evt.consume(); // establece a solo 8 numero
        }
        if ((car < '0' || car > '9')) { // solo acepta numeros
            evt.consume(); // consume si no son numeros
        }

    }//GEN-LAST:event_txtcod2KeyTyped

    private void bottbuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bottbuscarActionPerformed
        String impri = "";
        boolean cod1 = true;
        boolean cod2 = true;
        txtnombre1.setText("");
        txtejemplares1.setText("");
        txtestante1.setText("");
        txtnombre2.setText("");
        txtejemplares2.setText("");
        txtestante2.setText("");
        if (txtcod1.getText().length() > 0) {
            Connection conexion = null;
            try {
                conexion = conexionBI.conector();
                String sentencia_buscar = "select Titulo, Estante, NoEjem from libro where idlibro = " + txtcod1.getText() + ";";
                sentencia_preparada = (PreparedStatement) conexion.prepareStatement(sentencia_buscar);
                resultado = sentencia_preparada.executeQuery();
                if (resultado.next()) {
                    txtnombre1.setText(resultado.getString("Titulo"));
                    txtestante1.setText(resultado.getString("Estante"));
                    txtejemplares1.setText(resultado.getString("NoEjem"));
                    //conexion.close();
                    cod1 = false;
                    bottprestar.setEnabled(true);
                }
                conexion.close();
            } catch (Exception e) {
                System.out.println("busqueda id libro para prestamo  " + e);
            }
            txtcod11.setText(txtcod1.getText());
            try {
                int ejem = Integer.valueOf(txtejemplares1.getText());
            if (ejem <= 0) {
                this.label1.setText("no hay libros disponibles");
            }
            } catch (Exception e) {
            }
            
        }
        if (cod1 == true && txtcod1.getText().length() > 0) {
            impri = "libro 1º no encontrado ";
        }
        if (txtcod2.getText().length() > 0) {
            Connection conexion = null;
            try {
                conexion = conexionBI.conector();
                String sentencia_buscar = "select Titulo, Estante, NoEjem from libro where idlibro = " + txtcod2.getText() + ";";
                sentencia_preparada = (PreparedStatement) conexion.prepareStatement(sentencia_buscar);
                resultado = sentencia_preparada.executeQuery();
                if (resultado.next()) {
                    txtnombre2.setText(resultado.getString("Titulo"));
                    txtestante2.setText(resultado.getString("Estante"));
                    txtejemplares2.setText(resultado.getString("NoEjem"));
                    //conexion.close();
                    cod2 = false;
                    bottprestar.setEnabled(true);
                }
                conexion.close();
            } catch (Exception e) {
                System.out.println("2 busqueda 2id libro para prestamo  " + e);
            }
            txtcod22.setText(txtcod2.getText());
            int ejem = Integer.valueOf(txtejemplares2.getText());
            if (ejem <= 0) {
                this.label2.setText("no hay libros disponibles");
            }
        }
        if (cod2 == true && txtcod2.getText().length() > 0) {
            impri += "\nlibro 2º no encontrado";
        }
        if (impri.length() > 0) {
            JOptionPane.showMessageDialog(null, impri);
        }


    }//GEN-LAST:event_bottbuscarActionPerformed

    private void bottprestarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bottprestarActionPerformed
        // TODO add your handling code here:
        prestarlibro pre = new prestarlibro();
        pre.buscaralumno(matricu);
        pre.libros(txtcod11.getText(), txtcod22.getText());
        pre.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_bottprestarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(buscarlibroparaprestar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(buscarlibroparaprestar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(buscarlibroparaprestar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(buscarlibroparaprestar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new buscarlibroparaprestar().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton bottbuscar;
    private javax.swing.JToggleButton bottprestar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JToggleButton jToggleButton1;
    private javax.swing.JLabel label1;
    private javax.swing.JLabel label2;
    private javax.swing.JTextField txtcod1;
    private javax.swing.JTextField txtcod11;
    private javax.swing.JTextField txtcod2;
    private javax.swing.JTextField txtcod22;
    private javax.swing.JTextField txtejemplares1;
    private javax.swing.JTextField txtejemplares2;
    private javax.swing.JTextField txtestante1;
    private javax.swing.JTextField txtestante2;
    private javax.swing.JTextField txtnombre1;
    private javax.swing.JTextField txtnombre2;
    // End of variables declaration//GEN-END:variables
}

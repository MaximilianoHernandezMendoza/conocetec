/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conocetec.Administrador;

import conocetec.metodossql.conexionBI;
import conocetec.metodossql.metodos;
import static conocetec.metodossql.metodos.resultado;
import static conocetec.metodossql.metodos.sentencia_preparada;
import java.awt.Color;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.ImageIcon;
import java.sql.Connection;
import java.util.Random;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author maxiliano
 */
public class devolucion extends javax.swing.JFrame {

    public static conexionBI conexion = new conexionBI();
    public static conexionBI conexion1 = new conexionBI();
    public static PreparedStatement sentencia_preparada;
    public static ResultSet resultado;
    public static String sql;
    public static int resultado_numero = 0;

    String fecha1 = null;
    String fecha2 = null;

    /**
     * Creates new form devolucion
     */
    public devolucion() {
        initComponents();
        Calendar c2 = new GregorianCalendar();
        jDateChooser1.setCalendar(c2);
        iniciar();
    }

    void iniciar() {
        setLocationRelativeTo(null); // localiza la panalla en el centro :V
        this.getContentPane().setBackground(new Color(150, 201, 255)); // color de fondo

        ImageIcon img1 = new ImageIcon(getClass().getResource("/imagenes/flecha-curva-izquierda.png"));
        ImageIcon icon = new ImageIcon(img1.getImage().getScaledInstance((this.bottatras.getWidth()) / 4, this.bottatras.getHeight(), 1));
        this.bottatras.setIcon(icon);

        ImageIcon img4 = new ImageIcon(getClass().getResource("/imagenes/limpiar.png"));
        ImageIcon icon4 = new ImageIcon(img4.getImage().getScaledInstance((this.bottlimpiar.getWidth()) / 4, this.bottlimpiar.getHeight(), 1));
        this.bottlimpiar.setIcon(icon4);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        bottatras = new javax.swing.JButton();
        txtcod1 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtcod2 = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jlnombre = new javax.swing.JLabel();
        panelcamara = new JPanelWebCam.JPanelWebCam();
        bottdevolver = new javax.swing.JButton();
        bottlimpiar = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        txtmatricula = new javax.swing.JTextField();
        bottbuscar = new javax.swing.JToggleButton();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();

        jLabel1.setFont(new java.awt.Font("Trebuchet MS", 2, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Cod Libro:");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        bottatras.setText("Atras");
        bottatras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bottatrasActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Trebuchet MS", 2, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Cod Libro2:");

        jLabel3.setFont(new java.awt.Font("Trebuchet MS", 2, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Matricula:");

        jlnombre.setFont(new java.awt.Font("Trebuchet MS", 2, 14)); // NOI18N
        jlnombre.setForeground(new java.awt.Color(255, 255, 255));
        jlnombre.setText("--");

        panelcamara.setBorder(new javax.swing.border.MatteBorder(null));

        javax.swing.GroupLayout panelcamaraLayout = new javax.swing.GroupLayout(panelcamara);
        panelcamara.setLayout(panelcamaraLayout);
        panelcamaraLayout.setHorizontalGroup(
            panelcamaraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 192, Short.MAX_VALUE)
        );
        panelcamaraLayout.setVerticalGroup(
            panelcamaraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 183, Short.MAX_VALUE)
        );

        bottdevolver.setText("Devolver");
        bottdevolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bottdevolverActionPerformed(evt);
            }
        });

        bottlimpiar.setText("Limpirar");
        bottlimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bottlimpiarActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Trebuchet MS", 3, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Cod Libro1:");

        bottbuscar.setText("Buscar");
        bottbuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bottbuscarActionPerformed(evt);
            }
        });

        jDateChooser1.setDateFormatString("YYYY-MM-dd");
        jDateChooser1.setEnabled(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(bottatras))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(38, 38, 38)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))
                        .addGap(13, 13, 13)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtcod1, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtcod2, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jlnombre))
                            .addComponent(txtmatricula, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 102, Short.MAX_VALUE)
                .addComponent(panelcamara, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(62, 62, 62))
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 252, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(bottbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(bottdevolver, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(94, 94, 94)
                        .addComponent(bottlimpiar, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(19, 19, 19))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(bottatras, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(116, 116, 116)
                                .addComponent(jlnombre))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(28, 28, 28)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txtmatricula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel3))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel4)
                                    .addComponent(txtcod1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txtcod2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel2)))))
                    .addComponent(panelcamara, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bottlimpiar)
                    .addComponent(bottdevolver)
                    .addComponent(bottbuscar))
                .addGap(18, 18, 18)
                .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(21, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bottatrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bottatrasActionPerformed
        // TODO add your handling code here:
        pantalla_admi pan = new pantalla_admi();
        //  pan.setVisible(true);
        this.dispose();

    }//GEN-LAST:event_bottatrasActionPerformed

    private void bottbuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bottbuscarActionPerformed
        // TODO add your handling code here:
        Connection conexion = null;
        //int resultado = 0;
        try {
            conexion = conexionBI.conector();

            String sentencia_buscar = "select  foto  from alumnos where Matricula = " + txtmatricula.getText() + ";";
            sentencia_preparada = (PreparedStatement) conexion.prepareStatement(sentencia_buscar);
            resultado = sentencia_preparada.executeQuery();
            if (resultado.next()) {
                byte[] imagen = resultado.getBytes("foto");
                panelcamara.setImagen(imagen);
            }
            conexion.close();
        } catch (Exception e) {
            System.out.println("foto " + e);
        }

        try {
            conexion = conexionBI.conector();

            String sentencia_buscar = "select FechaSa, Libro_idlibro  from prestamo where Alumnos_Matricula = " + txtmatricula.getText() + ";";
            sentencia_preparada = (PreparedStatement) conexion.prepareStatement(sentencia_buscar);
            resultado = sentencia_preparada.executeQuery();
            if (resultado.next()) {
                txtcod1.setText(resultado.getString("Libro_idlibro"));
                fecha1 = resultado.getString("FechaSa");
                txtcod2.setText(resultado.getString("Libro_idlibro"));
                fecha2 = resultado.getString("FechaSa");
            }
            
            conexion.close();
        } catch (Exception e) {
            System.out.println("devoler " + e);
        }
        String pr = (((JTextField) jDateChooser1.getDateEditor().getUiComponent()).getText()); // imprime fecha en consola
        System.out.println(pr);// imprime fecha en pantalla
        char[] ch = pr.toCharArray();
        String diatem = "" + ch[8] + ch[9];
        String mestem = "" + ch[5] + ch[6];
        String anotem = "" + ch[0] + ch[1] + ch[2] + ch[3];
        int diaactual = Integer.parseInt(diatem);
        int mesactual = Integer.parseInt(mestem);
        int dtem1 =0 ;
        int mtem1=0;
        int atem1=0;
        int dtem2=0;
        int mtem2=0;
        int atem2=0;
                
        try {
        char[] cd = fecha1.toCharArray();
        String diatem1 = "" + ch[8] + ch[9];
        dtem1 = Integer.parseInt(diatem1);
        String mestem1 = "" + ch[5] + ch[6];
        mtem1 = Integer.parseInt(mestem1);
        String anotem1 = "" + ch[0] + ch[1] + ch[2] + ch[3];  
        atem1 = Integer.parseInt(anotem1);
        } catch (Exception e) {
        }
        try {
        char[] cd = fecha2.toCharArray();
        String diatem2 = "" + ch[8] + ch[9];
        dtem2 = Integer.parseInt(diatem2);
        String mestem2 = "" + ch[5] + ch[6];
        mtem2 = Integer.parseInt(mestem2);
        String anotem2 = "" + ch[0] + ch[1] + ch[2] + ch[3];
        atem2 = Integer.parseInt(anotem2);
        } catch (Exception e) {
        }
        int dias;
        if(mtem2 > mesactual || dtem2 > diaactual){
     //       Connection conexion = null;
        int resultado = 0;
        Random ra = new Random();

        String a = String.valueOf(ra.nextInt(99999));
        String sentencia_guardada = "insert into usuarios (cod_adeudos, monto, descripcion, Alumnos_Matricula)values (?,?,?,?);";
        try {
            // se registra la editorial
            conexion = conexionBI.conector();
            sentencia_preparada = conexion.prepareStatement(sentencia_guardada);
            sentencia_preparada.setString(1, a);
            sentencia_preparada.setString(2, "20");
            sentencia_preparada.setString(3, " ");
            sentencia_preparada.setString(4, txtmatricula.getText());

            resultado = sentencia_preparada.executeUpdate();
            sentencia_preparada.close();//cierra conexion
            conexion.close();
        } catch (Exception e) {
            System.out.println("guardarusuarioalumno " + e);
        }
        if(txtcod1.getText().equalsIgnoreCase(txtcod2.getText())){
                txtcod2.setText("");
            }
        }
    }//GEN-LAST:event_bottbuscarActionPerformed

   
    private void bottdevolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bottdevolverActionPerformed
        // TODO add your handling code here:
        metodos me = new metodos();
        if (txtcod1.getText().length() > 0 && txtcod2.getText().length() > 0) {
            int libro1 = me.vercandidadlibro(txtcod1.getText());
            libro1++;
            me.modificarlibro2(txtcod1.getText(), libro1);
            int libro2 = me.vercandidadlibro(txtcod2.getText());
            libro2++;
            me.modificarlibro2(txtcod2.getText(), libro2);
            me.eliminarpretsamo(txtcod1.getText(), txtmatricula.getText());
            me.eliminarpretsamo(txtcod2.getText(), txtmatricula.getText());
            JOptionPane.showMessageDialog(null, "Libro a sido devuelto");
        }
        else if (txtcod1.getText().length() > 0) {
            int libro1 = me.vercandidadlibro(txtcod1.getText());
            libro1++;
            me.modificarlibro2(txtcod1.getText(), libro1);
            me.eliminarpretsamo(txtmatricula.getText(), txtcod1.getText());
            JOptionPane.showMessageDialog(null, "Libro a sido devuelto");
        }
        else if (txtcod2.getText().length() > 0) {
            int libro2 = me.vercandidadlibro(txtcod2.getText());
            libro2++;
            me.modificarlibro2(txtcod2.getText(), libro2);
            me.eliminarpretsamo(txtcod2.getText(), txtmatricula.getText());
            JOptionPane.showMessageDialog(null, "Libro a sido devuelto");
        }
    }//GEN-LAST:event_bottdevolverActionPerformed

    private void bottlimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bottlimpiarActionPerformed
        // TODO add your handling code here:
        txtcod1.setText("");
        txtcod2.setText("");
        txtmatricula.setText("");
    }//GEN-LAST:event_bottlimpiarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(devolucion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(devolucion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(devolucion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(devolucion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new devolucion().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bottatras;
    private javax.swing.JToggleButton bottbuscar;
    private javax.swing.JButton bottdevolver;
    private javax.swing.JButton bottlimpiar;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jlnombre;
    private JPanelWebCam.JPanelWebCam panelcamara;
    private javax.swing.JTextField txtcod1;
    private javax.swing.JTextField txtcod2;
    private javax.swing.JTextField txtmatricula;
    // End of variables declaration//GEN-END:variables
}

package conocetec.Administrador;

import conocetec.metodossql.conexionBI;
import conocetec.metodossql.metodos;
import static conocetec.metodossql.metodos.resultado;
import static conocetec.metodossql.metodos.sentencia_preparada;
import java.awt.Color;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.swing.ImageIcon;
import javax.swing.table.DefaultTableModel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author maxiliano
 */
public class prestarlibro extends javax.swing.JFrame {

    java.util.Date fecha = new Date();
    String matricula = "";
    int diaprestamo = 2;
    int diaEn;
    int diaSa;
    int mes;
    int ano;
    String fein;
    String fefi;
    String usu = "rodri";

    /**
     * Creates new form prestarlibro
     */
    public prestarlibro() {
        initComponents();
        iniciar();
        buscaralumno(txtmatri.getText());
    }

    public void buscaralumno(String codi) {
        Connection conexion = null;
            //int resultado = 0;
            try {
                conexion = conexionBI.conector();
                
                String sentencia_buscar = "select  foto  from alumnos where Matricula = " + codi + ";";
                sentencia_preparada = (PreparedStatement) conexion.prepareStatement(sentencia_buscar);
                resultado = sentencia_preparada.executeQuery();
                if (resultado.next()) {
                    byte[] imagen = resultado.getBytes("foto");
                    panelcamara.setImagen(imagen);
                }
                conexion.close();
            }catch(Exception e){
                System.out.println("error en prestar libro " + e);
            }
                    
        
        
        txtmatri.setText(codi);
        metodos met = new metodos();
        int[] array = new int[5];
        array[4] = 1;
        String ade = (met.busquedaadeudo(txtmatri.getText()));
        if (ade.equalsIgnoreCase("no adeudo")) {
            jl10.setText("sin adeudo");
            array[1] = 1;
        } else if (ade.equalsIgnoreCase("adeudo")) {
            jl10.setText("con adeudo");
        }
        if (met.busquedalibrosprestar(txtmatri.getText()).equalsIgnoreCase("1")) {
            jl10.setText("solo puede sacar 1");
        }
        if (met.busquedalibrosprestar(txtmatri.getText()).equalsIgnoreCase("2")) {
            jl10.setText("Limite Maximo");
            bottbuscar.setEnabled(false);
            array[4] = 0;
        }
        String semestre = met.busquedasemestre(txtmatri.getText());
        String completo = met.busquedanombrealumno(txtmatri.getText());
        if ((completo.length()) > 0) {
            txtnombrecomleto.setText(completo);
            array[2] = 1;
        }
        if (semestre.length() > 0) {
            boxsemestre.setSelectedItem(semestre);
            array[3] = 1;
        }
        if (array[1] == 1 && array[2] == 1 && array[3] == 1 && array[4] ==1) {
            bottbuscar.setEnabled(true);
            bottprestar.setEnabled(false);
        }
    }

    public void libros(String libro1, String libro2) {
        this.txtcodtlibro1.setText(libro1);
        this.txtcodlibro2.setText(libro2);
        if (txtcodtlibro1.getText().length() > 0) {
            Connection conexion = null;
            try {
                conexion = conexionBI.conector();
                String sentencia_buscar = "select Titulo, Estante, NoEjem from libro where idlibro = " + txtcodtlibro1.getText() + ";";
                sentencia_preparada = (PreparedStatement) conexion.prepareStatement(sentencia_buscar);
                resultado = sentencia_preparada.executeQuery();
                if (resultado.next()) {
                    txtlibro1.setText(resultado.getString("Titulo"));
                    bottprestar.setEnabled(true);
                }
                conexion.close();
            } catch (Exception e) {
                System.out.println("busquedapara prestamo  " + e);
            }
        }
        if (txtcodlibro2.getText().length() > 0) {
            Connection conexion = null;
            try {
                conexion = conexionBI.conector();
                String sentencia_buscar = "select Titulo, Estante, NoEjem from libro where idlibro = " + txtcodlibro2.getText() + ";";
                sentencia_preparada = (PreparedStatement) conexion.prepareStatement(sentencia_buscar);
                resultado = sentencia_preparada.executeQuery();
                if (resultado.next()) {
                    txtlibro2.setText(resultado.getString("Titulo"));
                    bottprestar.setEnabled(true);
                }
                conexion.close();
            } catch (Exception e) {
                System.out.println("busquedapara prestamo 22 " + e);
            }
        }
    }

    void iniciar() {
        setLocationRelativeTo(null); // localiza la panalla en el centro :V
        this.getContentPane().setBackground(new Color(150, 201, 255)); // color de fondo

        ImageIcon img1 = new ImageIcon(getClass().getResource("/imagenes/flecha-curva-izquierda.png"));
        ImageIcon icon = new ImageIcon(img1.getImage().getScaledInstance((this.bottatras.getWidth()) / 4, this.bottatras.getHeight(), 1));
        this.bottatras.setIcon(icon);

        ImageIcon img3 = new ImageIcon(getClass().getResource("/imagenes/lupa.png"));
        ImageIcon icon3 = new ImageIcon(img3.getImage().getScaledInstance((this.bottbuscar.getWidth()) / 4, this.bottbuscar.getHeight(), 1));
        this.bottbuscar.setIcon(icon3);

        /*ImageIcon img4 = new  ImageIcon(getClass().getResource("/imagenes/cerrar.png"));
        ImageIcon icon4 = new ImageIcon(img4.getImage().getScaledInstance((this.bottcerrar.getWidth()), this.bottcerrar.getHeight(), 1));
        this.bottcerrar.setIcon(icon4);
        
        ImageIcon img5 = new  ImageIcon(getClass().getResource("/imagenes/minimizar.png"));
        ImageIcon icon5 = new ImageIcon(img5.getImage().getScaledInstance((this.bottminimizar.getWidth()), this.bottminimizar.getHeight(), 1));
        this.bottminimizar.setIcon(icon5);*/
        Calendar c2 = new GregorianCalendar();
        jDateChooser1.setCalendar(c2);
        txtcodtlibro1.setEnabled(false);
        txtcodlibro2.setEnabled(false);
        txtnombrecomleto.setEnabled(false);
        txtlibro2.setEnabled(false);
        txtlibro1.setEnabled(false);
        txtmatri.setEnabled(false);
        boxsemestre.setEnabled(false);

        jLabel13.setText(fecha.getHours() + ":" + fecha.getMinutes() + ":" + fecha.getSeconds());
        int dia = Integer.valueOf(Integer.toString(jDateChooser1.getCalendar().get(Calendar.DAY_OF_WEEK)));
        if (dia == 4 || dia == 5) {
            diaprestamo = 4;
        }
        String pr = (((JTextField) jDateChooser1.getDateEditor().getUiComponent()).getText()); // imprime fecha en consola
        System.out.println(pr);// imprime fecha en pantalla
        char[] ch = pr.toCharArray();
        String diatem = "" + ch[8] + ch[9];
        String mestem = "" + ch[5] + ch[6];
        String anotem = "" + ch[0] + ch[1] + ch[2] + ch[3];
        fein = anotem + "-" + mestem + "-" + diatem + " " + fecha.getHours() + ":" + fecha.getMinutes();
        try {
            diaSa = Integer.parseInt(diatem);
            mes = Integer.parseInt(mestem);
            ano = Integer.parseInt(anotem);
            incrementardia();
        } catch (Exception e) {
            System.out.println("error fecha");
        }

    }

    public void usuarios(String usuar) {
        usu = usuar;
    }

    public void incrementardia() {
        for (int i = 0; i != diaprestamo; i++) {
            diaSa++;
            incrementarmes();
            incrementarano();
        }
        fefi = ano + "-" + mes + "-" + diaSa;
        jlfechadelovucion.setText(fefi);
        System.out.println("salida " + diaSa + " : " + mes + " : " + ano);
    }

    public void incrementarmes() {
        if (mes == 1 && diaSa >= 31) {// enero 
            mes++;
            diaSa = 1;
        } else if (mes == 2 && diaSa >= 28) {//febrero
            mes++;
            diaSa = 1;
        } else if (mes == 3 && diaSa >= 31) {//marzo
            mes++;
            diaSa = 1;

        } else if (mes == 4 && diaSa >= 30) {// abril
            mes++;
            diaSa = 1;

        } else if (mes == 5 && diaSa >= 31) { // mayo
            mes++;
            diaSa = 1;

        } else if (mes == 6 && diaSa >= 30) {// junio
            mes++;
            diaSa = 1;

        } else if (mes == 7 && diaSa >= 31) {//julio
            mes++;
            diaSa = 1;

        } else if (mes == 8 && diaSa >= 31) {// agosto
            mes++;
            diaSa = 1;

        } else if (mes == 9 && diaSa >= 30) {// septiembre
            mes++;
            diaSa = 1;

        } else if (mes == 10 && diaSa >= 31) {//octubre
            mes++;
            diaSa = 1;

        } else if (mes == 11 && diaSa >= 30) {//noviembre
            mes++;
            diaSa = 1;

        } else if (mes == 12 && diaSa >= 31) {// diciembre
            mes++;
            diaSa = 1;

        }

    }

    public void incrementarano() {
        if (mes >= 12 && diaSa >= 31) {
            mes = 1;
            diaSa = 1;
            ano++;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jCheckBoxMenuItem1 = new javax.swing.JCheckBoxMenuItem();
        bottprestar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtmatri = new javax.swing.JTextField();
        txtcodtlibro1 = new javax.swing.JTextField();
        bottbuscar = new javax.swing.JButton();
        panelcamara = new JPanelWebCam.JPanelWebCam();
        jLabel7 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtnombrecomleto = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtcodlibro2 = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jl10 = new javax.swing.JLabel();
        boxsemestre = new javax.swing.JComboBox<>();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txtlibro1 = new javax.swing.JTextField();
        txtlibro2 = new javax.swing.JTextField();
        bottotroprestamo = new javax.swing.JToggleButton();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();
        jLabel10 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jlfechadelovucion = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        bottatras = new javax.swing.JButton();

        jCheckBoxMenuItem1.setSelected(true);
        jCheckBoxMenuItem1.setText("jCheckBoxMenuItem1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(1125, 700));
        setMinimumSize(new java.awt.Dimension(1125, 700));
        setUndecorated(true);
        setPreferredSize(new java.awt.Dimension(1125, 700));
        getContentPane().setLayout(null);

        bottprestar.setText("Prestar");
        bottprestar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bottprestarActionPerformed(evt);
            }
        });
        getContentPane().add(bottprestar);
        bottprestar.setBounds(430, 470, 280, 46);

        jLabel1.setFont(new java.awt.Font("Trebuchet MS", 2, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("N° Control:");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(80, 100, 100, 18);

        jLabel2.setFont(new java.awt.Font("Trebuchet MS", 2, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Codigo de Libro:");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(50, 160, 140, 18);

        txtmatri.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtmatriActionPerformed(evt);
            }
        });
        txtmatri.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtmatriKeyTyped(evt);
            }
        });
        getContentPane().add(txtmatri);
        txtmatri.setBounds(210, 100, 98, 30);

        txtcodtlibro1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtcodtlibro1ActionPerformed(evt);
            }
        });
        txtcodtlibro1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtcodtlibro1KeyTyped(evt);
            }
        });
        getContentPane().add(txtcodtlibro1);
        txtcodtlibro1.setBounds(210, 160, 98, 30);

        bottbuscar.setText("Buscar Libros");
        bottbuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bottbuscarActionPerformed(evt);
            }
        });
        getContentPane().add(bottbuscar);
        bottbuscar.setBounds(10, 470, 280, 42);

        panelcamara.setBorder(new javax.swing.border.MatteBorder(null));

        javax.swing.GroupLayout panelcamaraLayout = new javax.swing.GroupLayout(panelcamara);
        panelcamara.setLayout(panelcamaraLayout);
        panelcamaraLayout.setHorizontalGroup(
            panelcamaraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 158, Short.MAX_VALUE)
        );
        panelcamaraLayout.setVerticalGroup(
            panelcamaraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 178, Short.MAX_VALUE)
        );

        getContentPane().add(panelcamara);
        panelcamara.setBounds(920, 100, 160, 180);

        jLabel7.setFont(new java.awt.Font("Trebuchet MS", 2, 14)); // NOI18N
        jLabel7.setText("USUARIO: Administrador");
        getContentPane().add(jLabel7);
        jLabel7.setBounds(50, 560, 170, 30);

        jLabel4.setFont(new java.awt.Font("Trebuchet MS", 2, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Foto:");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(840, 100, 60, 30);

        jLabel5.setFont(new java.awt.Font("Trebuchet MS", 2, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("<html>Nombre Completo:<html>");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(410, 90, 80, 60);
        getContentPane().add(txtnombrecomleto);
        txtnombrecomleto.setBounds(510, 100, 290, 30);

        jLabel6.setFont(new java.awt.Font("Trebuchet MS", 2, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Semestre:");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(90, 290, 80, 18);
        getContentPane().add(txtcodlibro2);
        txtcodlibro2.setBounds(210, 220, 100, 30);

        jLabel8.setFont(new java.awt.Font("Trebuchet MS", 2, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Codigo del 2° Libro");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(30, 230, 180, 18);

        jLabel9.setFont(new java.awt.Font("Trebuchet MS", 2, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Estatus:");
        getContentPane().add(jLabel9);
        jLabel9.setBounds(420, 290, 80, 18);

        jl10.setFont(new java.awt.Font("Trebuchet MS", 2, 14)); // NOI18N
        jl10.setForeground(new java.awt.Color(255, 255, 255));
        jl10.setText("--");
        getContentPane().add(jl10);
        jl10.setBounds(510, 290, 200, 14);

        boxsemestre.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccionar", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" }));
        getContentPane().add(boxsemestre);
        boxsemestre.setBounds(220, 290, 100, 20);

        jLabel11.setFont(new java.awt.Font("Trebuchet MS", 2, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("<html>Nombre libro1:<html>");
        getContentPane().add(jLabel11);
        jLabel11.setBounds(410, 150, 80, 40);

        jLabel12.setFont(new java.awt.Font("Trebuchet MS", 2, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("<html>Nombre Libro 2:<html>");
        getContentPane().add(jLabel12);
        jLabel12.setBounds(400, 220, 90, 36);
        getContentPane().add(txtlibro1);
        txtlibro1.setBounds(510, 150, 290, 30);
        getContentPane().add(txtlibro2);
        txtlibro2.setBounds(510, 220, 290, 30);

        bottotroprestamo.setText("Otro Prestamo");
        bottotroprestamo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bottotroprestamoActionPerformed(evt);
            }
        });
        getContentPane().add(bottotroprestamo);
        bottotroprestamo.setBounds(810, 470, 280, 50);

        jDateChooser1.setDateFormatString("YYYY-MM-dd");
        jDateChooser1.setEnabled(false);
        getContentPane().add(jDateChooser1);
        jDateChooser1.setBounds(30, 350, 290, 30);

        jLabel10.setFont(new java.awt.Font("Trebuchet MS", 3, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Hora:");
        getContentPane().add(jLabel10);
        jLabel10.setBounds(740, 290, 37, 18);

        jLabel13.setFont(new java.awt.Font("Trebuchet MS", 2, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("--");
        getContentPane().add(jLabel13);
        jLabel13.setBounds(810, 290, 120, 18);

        jLabel14.setFont(new java.awt.Font("Trebuchet MS", 3, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("Fecha de Devolucion:");
        getContentPane().add(jLabel14);
        jLabel14.setBounds(400, 350, 150, 18);

        jlfechadelovucion.setFont(new java.awt.Font("Trebuchet MS", 3, 14)); // NOI18N
        jlfechadelovucion.setForeground(new java.awt.Color(255, 255, 255));
        jlfechadelovucion.setText("--");
        getContentPane().add(jlfechadelovucion);
        jlfechadelovucion.setBounds(600, 350, 140, 18);

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/estudiar.png"))); // NOI18N
        getContentPane().add(jLabel3);
        jLabel3.setBounds(330, 20, 530, 550);

        bottatras.setText("atras");
        bottatras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bottatrasActionPerformed(evt);
            }
        });
        getContentPane().add(bottatras);
        bottatras.setBounds(10, 20, 130, 40);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtmatriActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtmatriActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtmatriActionPerformed

    private void txtmatriKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmatriKeyTyped
        // TODO add your handling code here:
        // TODO add your handling code here:
        char car = evt.getKeyChar(); // usa el codigo ascci
        if (txtmatri.getText().length() >= 8) {
            evt.consume(); // establece a solo 8 numero
        }
        if ((car < '0' || car > '9')) { // solo acepta numeros
            evt.consume(); // consume si no son numeros
        }
    }//GEN-LAST:event_txtmatriKeyTyped

    private void txtcodtlibro1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcodtlibro1KeyTyped
        // TODO add your handling code here:
        char car = evt.getKeyChar(); // usa el codigo ascci
        if (txtcodtlibro1.getText().length() >= 8) {
            evt.consume(); // establece a solo 8 numero
        }
        if ((car < '0' || car > '9')) { // solo acepta numeros
            evt.consume(); // consume si no son numeros
        }
    }//GEN-LAST:event_txtcodtlibro1KeyTyped

    private void txtcodtlibro1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtcodtlibro1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtcodtlibro1ActionPerformed

    private void bottatrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bottatrasActionPerformed
        // TODO add your handling code here:
        pantalla_admi pan = new pantalla_admi();
        // pan.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_bottatrasActionPerformed

    private void bottbuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bottbuscarActionPerformed
        int cu = 0;
        if (txtmatri.getText().length() > 0) {
            buscarlibroparaprestar bus = new buscarlibroparaprestar();
            if (jl10.getText().equalsIgnoreCase("solo puede sacar 1")) {
                cu = 1;
            }
            if (jl10.getText().equalsIgnoreCase("sin adeudo")) {
                cu = 2;
            }
            if (jl10.getText().equalsIgnoreCase("con adeudo")) {
                bottbuscar.setEnabled(false);
            }
            bus.entramatri(txtmatri.getText(), cu);

            bus.setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_bottbuscarActionPerformed

    private void bottotroprestamoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bottotroprestamoActionPerformed
        buscar_alumno_para_prestamo bus = new buscar_alumno_para_prestamo();
        bus.boll(false);
        bus.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_bottotroprestamoActionPerformed

    private void bottprestarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bottprestarActionPerformed
        // la salida
        metodos me = new metodos();
        Random ra = new Random();

        String a = String.valueOf(ra.nextInt(99999));
        String b = String.valueOf(ra.nextInt(99999));
//        String dia = Integer.toString(jDateChooser1.getCalendar().get(Calendar.DAY_OF_MONTH));
//        String mes = Integer.toString(jDateChooser1.getCalendar().get(Calendar.MONTH));
//        String anio = Integer.toString(jDateChooser1.getCalendar().get(Calendar.YEAR));
//        String fecha = dia + "-" + mes + "-" + anio;
//        System.out.println(fecha);
        if (txtcodtlibro1.getText().length() > 0 && txtcodlibro2.getText().length() > 0) {
            me.prestar(a, fein, jlfechadelovucion.getText(), txtmatri.getText(), txtcodtlibro1.getText(), usu);
        me.prestar(b, fein, jlfechadelovucion.getText(), txtmatri.getText(), txtcodlibro2.getText(), usu);
            bottprestar.setEnabled(false);
            buscaralumno(txtmatri.getText());
            JOptionPane.showMessageDialog(null, "Libro Presatdo");
        } else if (txtcodtlibro1.getText().length() > 0) {
            me.prestar(a, fein, jlfechadelovucion.getText(), txtmatri.getText(), txtcodtlibro1.getText(), usu);
            bottprestar.setEnabled(false);
            buscaralumno(txtmatri.getText());
            JOptionPane.showMessageDialog(null, "Libro Presatdo");
        } else if (txtcodlibro2.getText().length() > 0) {
            me.prestar(b, fein, jlfechadelovucion.getText(), txtmatri.getText(), txtcodlibro2.getText(), usu);
            bottprestar.setEnabled(false);
            buscaralumno(txtmatri.getText());
            JOptionPane.showMessageDialog(null, "Libro Presatdo");
        }

    }//GEN-LAST:event_bottprestarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(prestarlibro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(prestarlibro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(prestarlibro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(prestarlibro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new prestarlibro().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bottatras;
    private javax.swing.JButton bottbuscar;
    private javax.swing.JToggleButton bottotroprestamo;
    private javax.swing.JButton bottprestar;
    private javax.swing.JComboBox<String> boxsemestre;
    private javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem1;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jl10;
    private javax.swing.JLabel jlfechadelovucion;
    private JPanelWebCam.JPanelWebCam panelcamara;
    private javax.swing.JTextField txtcodlibro2;
    private javax.swing.JTextField txtcodtlibro1;
    private javax.swing.JTextField txtlibro1;
    private javax.swing.JTextField txtlibro2;
    private javax.swing.JTextField txtmatri;
    private javax.swing.JTextField txtnombrecomleto;
    // End of variables declaration//GEN-END:variables
}
